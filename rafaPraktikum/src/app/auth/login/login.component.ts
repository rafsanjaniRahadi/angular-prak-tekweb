import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { FormControl } from "@angular/forms"
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: any={};
  hide: boolean = true;

  constructor(
    public api: ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }
  login(){
    this.api.login(this.user.email, this.user.password).subscribe(res => {
      localStorage.setItem('appToken', JSON.stringify(res));
      this.router.navigate(['admin/dashboard']);
    },eror => {
      alert('Tidak dapat login');
    });
  }

}
