import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']

})

export class FileUploaderComponent implements OnInit {

  fileName = '';

  constructor(
    public api: ApiService,
    public dialogRef: MatDialogRef<FileUploaderComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    
  ) { }

  ngOnInit(): void {
  }

selectedFile:any;
onFileChange(event:any):void {
  if(event.target.files.length > 0) {
      this.selectedFile=event.target.files[0];
      console.log(this.selectedFile);
  }
  }

  loadingUpload: boolean = false;
 uploadFile() {
   let input = new FormData();
   input.append('file', this.selectedFile);
   this.loadingUpload = true;
   this.api.upload(input).subscribe(data=>{
     this.updateProduct(data);
     console.log(data);
   },error=>{
       this.loadingUpload = false;
       alert('Gagal mengunggah file');
   });
 }


updateProduct(data: any)
 {
   if(data.status == true)
   {
     /*lakukan update data produk disini
      this.api.put('/bookswithauth',this.data).subscribe(result=>{
        this.dialogRef.close(result);
        this.loadingUpload=false;
        alert('File berhasil diunggah');
         },erorr=>{
        */
        this.loadingUpload=false;
        this.dialogRef.close();
        alert('File berhasil diunggah');
   }else{
     alert(data.massage);
   }
 }

 updateBook(data: { url: any; })
 {
   this.api.put('book/'+this.dialogData.id, {url: data.url}).subscribe(res=>{
     console.log(res)
   })
 }
}